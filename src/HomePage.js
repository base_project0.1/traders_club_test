import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Api } from 'lib'
import { actions as CarsActions } from 'reducers/Cars'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './style'

import Header from 'components/Pages/Header'
import CarDetail from 'components/Pages/CarDetail'
import CarsList from 'components/Pages/CarsList'

import AddModal from 'components/Modals/AddModal'
import UpdateModal from 'components/Modals/UpdateModal'
import DeleteModal from 'components/Modals/DeleteModal'

import Pagination from 'components/Pagination'

import Loading from 'components/Loading'

import 'fontAwesome'

//http://www.redbitdev.com/getting-started-with-react-flexbox-grid/
// lg – 75em (most often 1200px) and up
//
// md – 64em (most often 1024px) to 74em
//
// sm – 48em (most often 768px) to 63em
//
// xs – up to 47em

class HomePage extends React.Component {

  constructor(props) {
    super(props)

    const bindByName = [
      "changePage",

      "getCarDetail",
      "setCarFilter",

      "toggleModalAdd",
      "toggleModalUpdate",
      "toggleModalDelete",

      "addCar",
      "updateCar",
      "deleteCar",
    ]

    bindByName.map((bind) => {
      this[bind] = this[bind].bind(this)
    })

    this.state = {
      showModal: false,
      showModalAdd: false,
      showModalUpdate: false,
      showModalDelete: false,
    }

    this.pagination = {
      currentPage: 0,
      qttPerPage: 10,
    }

    this.updateScreenSize()
    this.smallScreen = this.isSmallScreen()
  }

  isSmallScreen() { return this.cWidth < 768/*598 614*/ }

  updateScreenSize() { this.cWidth = window.outerWidth }

  onResize() {
    this.updateScreenSize()
    if (this.smallScreen !== this.isSmallScreen()) {
      this.smallScreen = this.isSmallScreen()
      this.forceUpdate()
    }
    console.log("isSmallScreen", this.smallScreen, this.cWidth);
  }

  async componentDidMount() {
    await Api.init()
    await this.getCarsList()
    window.addEventListener('resize', () => this.onResize())
  }

  changePage(page) {
    this.props.actions.unsetCarDetail()
    this.pagination.currentPage = page
    this.forceUpdate()
  }

  toggleModalAdd() {
    this.setState({
      showModalAdd: !this.state.showModalAdd
    })
  }

  toggleModalUpdate() {
    this.setState({
      showModalUpdate: !this.state.showModalUpdate
    })
  }

  toggleModalDelete() {
    this.setState({
      showModalDelete: !this.state.showModalDelete
    })
  }

  getCarsList() {
    this.props.actions.getCarsList(
      () => this.forceUpdate(),
      e => console.error("ERROR getCarsList...", e)
    )
  }

  getCarDetail(id) {
    this.props.actions.getCarDetail(
      id,
      () => this.forceUpdate(),
      e => console.error("ERROR getCarDetail...", e)
    )
  }

  setCarFilter(filter) {
    this.props.actions.setCarFilter(
      filter,
      () => this.forceUpdate(),
      e => console.error("ERROR setCarFilter...", e)
    )
  }

  addCar(newCar) {
    this.props.actions.addCar(
      newCar,
      () => {
        this.toggleModalAdd()
        this.forceUpdate()
      },
      e => console.error("ERROR addCar...", e)
    )
  }

  updateCar(id, car) {
    this.props.actions.updateCar(
      id,
      car,
      () => {
        this.getCarDetail(id)
        this.toggleModalUpdate()
      },
      e => console.error("ERROR updateCar...", e)
    )
  }

  deleteCar(id) {
    this.props.actions.deleteCar(
      id,
      () => {
        this.toggleModalDelete()
        this.getCarsList()
      },
      e => console.error("ERROR deleteCar...", e)
    )
  }

  render() {

    const carsList = this.props.carsList || []
    const carDetail = this.props.carDetail && this.props.carDetail._id && this.props.carDetail

    const totalPages = Math.ceil(carsList.length / this.pagination.qttPerPage)

    const { currentPage, qttPerPage } = this.pagination

    return (
      <Style>
        <Loading isLoading={this.props.isLoading} />
        {
          carsList &&
          <Pagination
            onPageChange={this.changePage}
            pageCount={totalPages}
            forcePage={this.pagination.currentPage}
          />
        }
        {
          this.state.showModalAdd &&
          <AddModal
            show={this.state.showModalAdd}
            addCar={this.addCar}
            toggleModal={this.toggleModalAdd}
          />
        }

        {
          carDetail && this.state.showModalUpdate &&
          <UpdateModal
            show={this.state.showModalUpdate}
            car={carDetail}
            updateCar={this.updateCar}
            toggleModal={this.toggleModalUpdate}
          />
        }
        {
          carDetail && this.state.showModalDelete &&
          <DeleteModal
            show={this.state.showModalDelete}
            car={carDetail}
            deleteCar={this.deleteCar}
            toggleModal={this.toggleModalDelete}
          />
        }
        <div className="mainContainer">
          <Header
            setCarFilter={this.setCarFilter}
            toggleModalAdd={this.toggleModalAdd}
          />
          <Grid fluid>
            <Row>
              <CarsList
                carsList={carsList}
                currentCar={carDetail}
                getCardDetail={this.getCarDetail}
                rangeList={{
                  page: currentPage,
                  qttPage: qttPerPage,
                }}
                toggleModalUpdate={this.toggleModalUpdate}
                toggleModalDelete={this.toggleModalDelete}

                isSmallScreen={this.smallScreen}
              />
              {
                carDetail && !this.smallScreen &&
                <Col xs={0} sm={6} md={6} lg={6}>
                  <CarDetail
                    carDetail={carDetail}
                    toggleModalUpdate={this.toggleModalUpdate}
                    toggleModalDelete={this.toggleModalDelete}
                  />
                </Col>
              }
            </Row>
          </Grid>
        </div>
      </Style>
    )
  }
}

function mapStateToProps({ cars }) {
  const { carsList, carDetail, carsQtt, isLoading } = cars

  return {
    carsList,
    carDetail,
    carsQtt,
    isLoading,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...CarsActions,
    }, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
