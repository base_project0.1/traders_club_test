import HomePage from './HomePage'

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { BrowserRouter, Route, Link } from "react-router-dom"

import AppReducer from './reducers'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';

// import express from 'express'
// import cors from 'cors'
// const app = express()
// app.options('*', cors())

// app.use('/static', express.static(path.join(process.cwd(), '.build')));

let store = createStore(
  AppReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk),
)

function renderApp() {
  ReactDOM.render(
    <Provider store={store}>
      <AppContainer>
        <div>
          <BrowserRouter>
            <div>
              <Route exact path="/" component={HomePage} />
            </div>
          </BrowserRouter>
        </div>
      </AppContainer>
    </Provider>
    ,
    document.querySelector('[data-js="app"]')
  )
}

let isFirst = false
if (module.hot || isFirst) {//apenas em modo de desenvolvimento
  renderApp()
  isFirst = false
}
