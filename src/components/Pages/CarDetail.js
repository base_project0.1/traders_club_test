import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './styles/carDetailStyle'

import 'fontAwesome'

class CarDetail extends React.Component {

  render() {

    const carDetail = this.props.carDetail

    return (
      <Style>
        <div className="title">Detalhes</div>
        <div className={"cardDetail ".concat(carDetail.vendido ? " sold " : "for-sale ")}>
          <div className="carName">{carDetail.veiculo}</div>
          <Row>
            <Col xs={6} sm={6} md={6} lg={6}>
              <div><strong>Marca</strong></div>
              <div className="carBrand">{carDetail.marca}</div>
            </Col>
            <Col xs={6} sm={6} md={6} lg={6}>
              <div><strong>Ano</strong></div>
              <div>{carDetail.ano || "Sem ano"}</div>
            </Col>
          </Row>
          <div className="desc">{carDetail.descricao || "Sem descrição."}</div>
          <hr />
          <button onClick={this.props.toggleModalUpdate}>
            <i className="fa fa-pencil" aria-hidden="true" />Editar
          </button>
          <button onClick={this.props.toggleModalDelete}>
            <i className="fa fa-eraser" aria-hidden="true" />Apagar
          </button>
          {
            !this.props.hideTag &&
            <div className="tag right" title={carDetail.vendido ? "Vendido!" : "À Venda"}>
              <i className="fa fa-tag fa-2x" aria-hidden="true" />
            </div>
          }
        </div>
      </Style>
    )
  }
}

CarDetail.propTypes = {
  carDetail: PropTypes.object.isRequired,
  toggleModalUpdate: PropTypes.func,
  toggleModalDelete: PropTypes.func,
  hideTag: PropTypes.bool,
}

export default CarDetail
