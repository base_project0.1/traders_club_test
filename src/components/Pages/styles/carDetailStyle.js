import styled from 'styled-components'

const style = styled.div`

  margin: 10px;

  top: 0px;
  position: sticky;

  .cardDetail {
    background-color: #f1f2f0;
    padding: 10px;
  }

  .title {
    padding-top: 10px;
    padding-bottom: 15px;
    letter-spacing: 0em;
  }

  .sold {
    opacity: 0.8;
    .tag i {
      color: #ff000057;
    }
  }

  .for-sale .tag i {
    color: #85ae9d;
  }

  .carBrand {
    text-transform: uppercase;
  }

  .carName {
    color: #85ae9d;
    margin-bottom: 35px;
  }

  .desc {
    margin-top: 35px;
    margin-bottom: 35px;
  }

  .right {
    float: right;
  }

`
export default style
