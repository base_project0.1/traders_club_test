import styled from 'styled-components'

const style = styled.div`

  padding-left: 10px;

  .title {
    padding-top: 10px;
    padding-bottom: 15px;
    letter-spacing: 0em;
  }

  .line {
    background-color: white;
    margin: 5px;
    padding: 5px;
    cursor: pointer;
  }

  .selected {
    background-color: #f1f2f0;
  }

  .sold {
    opacity: 0.8;
    i {
      color: #ff000057;
    }
  }

  .for-sale .tag i {
    color: #85ae9d;
  }

  .carBrand {
    text-transform: uppercase;
  }

  .carName {
    color: #85ae9d;
  }

`
export default style
