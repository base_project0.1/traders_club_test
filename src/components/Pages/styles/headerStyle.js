import styled from 'styled-components'

const style = styled.div`

  .mainHeader {
    background-color: #364147;
    display: flow-root;
    padding: 20px;
  }

  .i-div {
      display: inline-flex;
      cursor: pointer;
  }

  .logo {
    display: inline-flex;
    float: left;
    color: white;
    -webkit-align-items: center;
    align-items: center;
    -webkit-justify-content: center;
    justify-content: center;
  }

  .filter {
    display: inline-flex;
    float: right;
    .i-div {
      background-color: white;
      padding: 5px;
    }
  }

  .pageTitle {
    padding: 10px;
    padding-top: 50px;
    display: flow-root;
    .title {
      display: inline-flex;
      float: left;
    }
    .i-div {
      display: inline-flex;
      float: right;
    }
  }
`
export default style
