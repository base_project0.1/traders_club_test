import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './styles/headerStyle'

import 'fontAwesome'

class Header extends React.Component {

  constructor(props) {
    super(props)

    this.filter = null
  }

  render() {

    return (
      <Style>
        <div>
          <div className="mainHeader">
            <div className="logo">
              <div className="headerTitle title">
                <div className="i-div">
                  <i className="fa fa-car" aria-hidden="true" />
                </div>
                Traders Club Garage
              </div>
            </div>
            <div className="filter">
              <input
                type="text"
                placeholder="Busca por um veículo"
                onChange={e => {this.filter = e.target.value}}
              />
              <div className="i-div">
                <i
                  className="fa fa-search"
                  aria-hidden="true"
                  onClick={() => this.props.setCarFilter(this.filter)}
                />
              </div>
            </div>
          </div>
          <div className="pageTitle">
            <div className="title">Veículos</div>
            <div className="i-div" onClick={this.props.toggleModalAdd}>
              <i className="fa fa-plus-circle fa-2x" aria-hidden="true" />
            </div>
          </div>
          <hr />
        </div>
      </Style>
    )
  }
}

Header.propTypes = {
  setCarFilter: PropTypes.func.isRequired,
  toggleModalAdd: PropTypes.func,
}

export default Header
