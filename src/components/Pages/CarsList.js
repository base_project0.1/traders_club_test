import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'
import Style from './styles/carsListStyle'

import CarDetail from './CarDetail'

import 'fontAwesome'

class CarsList extends React.Component {

  render() {

    const carsList = this.props.carsList
    const currentCarId = this.props.currentCar && this.props.currentCar._id
    const range = this.props.rangeList
    const min = (range.page && range.qttPage) && (range.page * range.qttPage) || 0
    const max = (range.page && range.qttPage) && (min + range.qttPage) || 10

    return (
      <Col xs={12} sm={6} md={6} lg={6}>
        <Style>
          <div className="title">Lista de Veículos</div>
          {
            carsList.slice(min, max).map( (car) =>
              (
                <div
                  className={
                    "line " +
                    (car.vendido ? " sold " : "for-sale ") +
                    (currentCarId === car._id ? "selected " : " ")
                  }
                  key={car._id} onClick={() => this.props.getCardDetail(car._id)}
                >
                  <Row>
                    <Col xs={11} sm={11} md={11} lg={11}>
                      <div className="carBrand">{car.marca}</div>
                      <div className="carName">{car.veiculo}</div>
                      <div>{car.ano || "Sem ano"}</div>
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1}>
                      <div className="tag" title={car.vendido ? "Vendido!" : "À Venda"}>
                        <i className="fa fa-tag fa-2x" aria-hidden="true" />
                      </div>
                    </Col>
                  </Row>
                  {
                    currentCarId === car._id && this.props.isSmallScreen &&
                    <Col xs={12} sm={12} md={12} lg={12}>
                      <CarDetail
                        carDetail={this.props.currentCar}
                        toggleModalUpdate={this.props.toggleModalUpdate}
                        toggleModalDelete={this.props.toggleModalDelete}

                        hideTag={true}
                      />
                    </Col>
                  }
                </div>
              )
            )
          }
        </Style>
      </Col>
    )
  }
}

CarsList.propTypes = {
  carsList: PropTypes.array.isRequired,
  currentCar: PropTypes.object,
  getCardDetail: PropTypes.func,
  rangeList: PropTypes.object,

  toggleModalUpdate: PropTypes.func,
  toggleModalDelete: PropTypes.func,

  isSmallScreen: PropTypes.bool,
}

export default CarsList
