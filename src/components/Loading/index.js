import React from 'react'
import PropTypes from 'prop-types'

import loadingGif from './loading.gif'
import StyledLoading from './styledLoading'

const Loading = ({ isLoading }) => (
  isLoading &&
  <StyledLoading>
    <img style={{ width: 'auto', height: 'auto' }} src={loadingGif} />
  </StyledLoading>
)

Loading.propTypes = {
  isLoading: PropTypes.bool.isRequired
}

export default Loading
