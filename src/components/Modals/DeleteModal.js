import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Grid, Row, Col } from 'react-flexbox-grid'

import Modal from './DefaultModal'

import 'fontAwesome'

class DeleteModal extends React.Component {

  constructor(props) {
    super(props)
    this.carObj = {
      veiculo: props.car.veiculo,
      marca: props.car.marca,
      ano: props.car.ano,
      vendido: props.car.vendido,
      descricao: props.car.descricao,
    }
  }

  render() {

    const car = this.props.car

    return (
      <Modal
        show={this.props.show}
        toggleModal={this.props.toggleModal}
      >
        <div className="title">Deletar Veículo</div>
        <div className="sub-title">Realmete desaja excluir permanentemente os registro deste veículo?</div>
        <hr />
        <div><strong>Id:</strong> {car._id}</div>
        <div><strong>Código FIPE:</strong> {car.cod_fipe}</div>
        <div><strong>Marca:</strong> {car.marca}</div>
        <div><strong>Veículo:</strong> {car.veiculo}</div>
        <div><strong>Situação:</strong> {car.vendido ? "Vendido" : "À venda"}</div>
        <div><strong>Descrição:</strong> {car.descricao || "Sem descrição."}</div>
        <hr />
        <button className="right" onClick={this.props.toggleModal}>Não</button>
        <button className="right" onClick={() => this.props.deleteCar(car._id)}>Sim</button>
      </Modal>
    )
  }
}

DeleteModal.propTypes = {
  show: PropTypes.bool,
  car: PropTypes.object.isRequired,
  deleteCar: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired,
}

export default DeleteModal
