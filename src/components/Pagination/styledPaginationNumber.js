import styled from 'styled-components'

const styledPaginationNumber = styled.div`

  top: 50%;
  transform: translate(0, -50%);

  background-color: #364147;
  border-radius: 3px;

  color:white;
  font-weight: bolder;

  position: fixed;
  left: 0px;

  padding: 3px;
  margin: 0;

  ul {
    list-style: none;
    padding: 0;
    margin: 0;
    font-size: smaller;
    text-align: center;
  }

  .selectedPage {
    background-color: #85ae9d;
    border-radius: 3px;
  }

  ${'' /* background-color: #477dc0;
  color: #fff;
  border: #b1b1b1 1px solid;
  border-radius: 10px;
  box-shadow: 0px 3px 0px 0px #0000004a;

  display: flex;
  align-content: center;
  text-align: center;
  justify-content: center;

  padding: 0;
  width: 30%;
  height: 20px;

  i {
    justify-content: center;
    display: flex;
    padding: 10px;
    color: #f0f0ff;
  }

  .pagination, ul, li{
    padding: 0;
  }

  li{
    display: inline-block;
    cursor: pointer;
    padding: 0px;
    border-radius: 3px;
    box-shadow: 0px 3px 0px 0px #0000004a;
    border: #00000021 1px solid;
  }

  .break-me .selectedPage {
    cursor: context-menu;
    outline-style: none;
  }

  .selectedPage {
    cursor: context-menu;
    background-color: #2f4296;
    color: black
    border: black 1px solid;
    border-radius: 3px;
    text-decoration: none;
  } */}

`
export default styledPaginationNumber
