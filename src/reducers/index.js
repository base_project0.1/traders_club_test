import { combineReducers } from 'redux'

import cars from './Cars'

export default combineReducers({
  cars,
})
