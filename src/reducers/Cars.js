import { Api } from 'lib'

const initialState = {
  carsList: [],
  carsQtt: 0,
  carDetail: {},
  isLoading: false,
}

const types = {
  FETCH_CARS_LIST_SUCCESS: 'FETCH_CARS_LIST_SUCCESS',
  FETCH_CARS_LIST_ERROR: 'FETCH_CARS_LIST_ERROR',

  FETCH_CAR_DETAIL_SUCCESS: 'FETCH_CAR_DETAIL_SUCCESS',
  FETCH_UNSET_CAR_DETAIL: 'FETCH_UNSET_CAR_DETAIL',
  FETCH_CAR_DETAIL_ERROR: 'FETCH_CAR_DETAIL_ERROR',

  FETCH_FILTER_SUCCESS: 'FETCH_FILTER_SUCCESS',
  FETCH_FILTER_ERROR: 'FETCH_FILTER_ERROR',

  FETCH_ADD_CAR_SUCCESS: 'FETCH_ADD_CAR_SUCCESS',
  FETCH_ADD_CAR_ERROR: 'FETCH_ADD_CAR_ERROR',

  FETCH_UPDATE_CAR_SUCCESS: 'FETCH_UPDATE_CAR_SUCCESS',
  FETCH_UPDATE_CAR_ERROR: 'FETCH_UPDATE_CAR_ERROR',

  FETCH_DELETE_CAR_SUCCESS: 'FETCH_DELETE_CAR_SUCCESS',
  FETCH_DELETE_CAR_ERROR: 'FETCH_DELETE_CAR_ERROR',

  REQUEST_START: 'REQUEST_START',
  REQUEST_FINISHED: 'REQUEST_FINISHED',
}
export const actions = {
  getCarsList: (success, error) => (dispatch) => {
    dispatch({type: types.REQUEST_START})
    return Api.get(`veiculos`)
      .then(response =>
        dispatch({
          type: types.FETCH_CARS_LIST_SUCCESS,
          payload: response.data,
        }))
      .then(success)
      .catch((err) => {
        error && error(err)
        dispatch({ type: types.REQUEST_FINISHED })
      })
  },

  getCarDetail: (id, success, error) => (dispatch) => {
    dispatch({type: types.REQUEST_START})
    return Api.get(`veiculos/${id}`)
      .then(response =>
        dispatch({
          type: types.FETCH_CAR_DETAIL_SUCCESS,
          payload: response.data,
        }))
      .then(success)
      .catch((err) => {
        error && error(err)
        dispatch({ type: types.REQUEST_FINISHED })
      })
  },

  unsetCarDetail: () => (dispatch) => {
    return dispatch({ type: types.FETCH_UNSET_CAR_DETAIL })
  },

  setCarFilter: (filter, success, error) => (dispatch) => {
    dispatch({type: types.REQUEST_START})
    return Api.get(`veiculos/?filters=veiculo@${filter}`)
      .then(response =>
        dispatch({
          type: types.FETCH_FILTER_SUCCESS,
          payload: response.data,
        }))
      .then(success)
      .catch((err) => {
        error && error(err)
        dispatch({ type: types.REQUEST_FINISHED })
      })
  },

  addCar: (car, success, error) => (dispatch) => {
    dispatch({type: types.REQUEST_START})
    return Api.post(`veiculos`, car)
      .then(response =>
        dispatch({
          type: types.FETCH_ADD_CAR_SUCCESS,
          payload: response.data,
        }))
      .then(success)
      .catch((err) => {
        error && error(err)
        dispatch({ type: types.REQUEST_FINISHED })
      })
  },

  updateCar: (id, car, success, error) => (dispatch) => {
    dispatch({type: types.REQUEST_START})
    return Api.put(`veiculos/${id}`, car)
      .then(response =>
        dispatch({
          type: types.FETCH_UPDATE_CAR_SUCCESS,
          payload: response.data,
        }))
      .then(success)
      .catch((err) => {
        error && error(err)
        dispatch({ type: types.REQUEST_FINISHED })
      })
  },

  deleteCar: (id, success, error) => (dispatch) => {
    dispatch({type: types.REQUEST_START})
    return Api.delete(`veiculos/${id}`)
      .then(response =>
        dispatch({
          type: types.FETCH_UPDATE_CAR_SUCCESS,
          payload: response.data,
        }))
      .then(success)
      .catch((err) => {
        error && error(err)
        dispatch({ type: types.REQUEST_FINISHED })
      })
  },

}
export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.FETCH_CARS_LIST_SUCCESS:
      return {
        ...state,
        carsList: payload,
        carsQtt: payload.length,
        carDetail: null,
        isLoading: false,
      }

    case types.FETCH_CAR_DETAIL_SUCCESS:
      return {
        ...state,
        carDetail: payload,
        isLoading: false,
      }

    case types.FETCH_UNSET_CAR_DETAIL:
      return {
        ...state,
        carDetail: null,
      }

    case types.FETCH_FILTER_SUCCESS:
      return {
        ...state,
        carsList: payload,
        carDetail: null,
        isLoading: false,
      }

    case types.FETCH_ADD_CAR_SUCCESS:
      return {
        ...state,
        // carsList: payload,
        isLoading: false,
      }

    case types.FETCH_UPDATE_CAR_SUCCESS:
      return {
        ...state,
        // carsList: payload,
        isLoading: false,
      }

    case types.FETCH_DELETE_CAR_SUCCESS:
      return {
        ...state,
        carDetail: null,
        isLoading: false,
      }

    case types.REQUEST_START:
      return {
        ...state,
        isLoading: true,
      }
    default:
      return state
  }
}
