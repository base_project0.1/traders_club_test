import styled from 'styled-components'

const style = styled.div`
  position: relative;
  border: #6a737d 1px solid;
  box-shadow: 0px 1px 0px 0px #0000004a;
  padding: 2px;

  font-family: Lato;

  .mainContainer {
    background-color: #e2e4e1;
  }

${'' /* Genetal Styles */}
  .title {
    text-transform: uppercase;
    font-family: Montserrat;
    font-weight: bolder;
    letter-spacing: 0.5em;
  }

  .container-fluid, .row, div[class^='col-'] {
    padding: 0px;
    margin: 0px;
  }

  button {
    min-width: 80px;
    min-height: 40px;

    background: #45535a;
    border: 1px solid #00000054;
    color: white;
    font-weight: bolder;
    text-transform: uppercase;

    padding: 5px;
    margin: 10px;

    cursor: pointer;
  }

  hr {
    border-color: grey;
  }

`
export default style
